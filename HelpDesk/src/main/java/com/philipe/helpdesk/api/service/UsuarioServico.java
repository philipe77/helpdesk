package com.philipe.helpdesk.api.service;

import org.springframework.data.domain.Page;

import com.philipe.helpdesk.api.entidade.Usuario;

public interface UsuarioServico {

	Usuario findByEmail(String email);
	Usuario createOrUpdate(Usuario usuario);
	
	Usuario findById(String id);
	
	void delete(String id);
	
	Page<Usuario> findAll(int page, int count);
	
	
}
