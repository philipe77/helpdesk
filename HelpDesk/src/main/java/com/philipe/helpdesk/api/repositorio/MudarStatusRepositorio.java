package com.philipe.helpdesk.repositorio;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.philipe.helpdesk.api.entidade.MudarStatus;

public interface MudarStatusRepositorio  extends MongoRepository<MudarStatus, String>{
	
	Iterable<MudarStatus> findByTicketIdOrderByDataMudouStatusDesc(String  tickedId);

}
