package com.philipe.helpdesk.api.enums;

public enum StatusEnum {

	Novo,
	Assinado,
	Resolvido,
	Aprovado,
	Reprovado,
	Fechado;
	
	public static StatusEnum getStatus(String status) {
		switch (status) {
		case "Novo": return Novo;
		case "Assinado":return Assinado;
		case "Resolvido":return Resolvido;
		case "Aprovado":return Aprovado;
		case "Reprovado":return Reprovado;
		case "Fechado":return Fechado;
			
		default:
			return Novo;
		}
	}
}
