package com.philipe.helpdesk.repositorio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.philipe.helpdesk.api.entidade.Ticket;

public interface TicketRepositorio extends MongoRepository<Ticket, String>{

	Page<Ticket> findByUsuarioIdOrderByDataDesc(Pageable pages, String userId);

	Page<Ticket> findByTituloIgnoreCaseContainingAndStatusAndPrioridadeOrderByDataDesc(
			String titulo, String status, String prioridade,
			Pageable pages);
	
	
	Page<Ticket> findByTituloIgnoreCaseContainingAndStatusAndPrioridadeAndUsuarioIdOrderByDataDesc(String titulo,
			String status, String prioridade,Pageable pages
			);
	
	Page<Ticket> findByTituloIgnoreCaseContainingAndStatusAndPrioridadeAndAceitaTicketOrderByDataDesc(String titulo,
			String status, String prioridade,Pageable pages
			);
	
	Page<Ticket> findByNumero(Integer numero, Pageable pages);
	
}
