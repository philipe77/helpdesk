package com.philipe.helpdesk.repositorio;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.philipe.helpdesk.api.entidade.Usuario;

public interface UsuarioRepositorio extends MongoRepository<Usuario,String>{

	Usuario findByEmail(String email); 
}
