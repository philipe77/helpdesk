package com.philipe.helpdesk.api.enums;

public enum PrioridadeEnum {
	Alta,
	Normal,
	Baixa
}
