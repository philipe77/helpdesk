package com.philipe.helpdesk.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.philipe.helpdesk.api.entidade.Usuario;
import com.philipe.helpdesk.api.service.UsuarioServico;
import com.philipe.helpdesk.repositorio.UsuarioRepositorio;

@Service
public class UsuarioServicoImp implements UsuarioServico{
	
	@Autowired
	private UsuarioRepositorio usuarioRep;
	
	@Override
	public Usuario findByEmail(String email) {
		
		return this.usuarioRep.findByEmail(email);
	}

	@Override
	public Usuario createOrUpdate(Usuario usuario) {

		return this.usuarioRep.save(usuario);
	}

	@Override
	public Usuario findById(String id) {
		
		return this.usuarioRep.findOne(id);
	}

	@Override
	public void delete(String id) {
		this.usuarioRep.delete(id);
	}

	@Override
	public Page<Usuario> findAll(int page, int count) {
		Pageable pages = new PageRequest(page, count);
		return this.usuarioRep.findAll(pages);
	}
	
}
