package com.philipe.helpdesk.api.entidade;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.philipe.helpdesk.api.enums.StatusEnum;

@Document
public class MudarStatus {
	@Id
	private String id;
	
	@DBRef
	private Ticket ticket;
	
	@DBRef
	private Usuario usuarioMudouStatus;
	
	private Date dataMudouStatus;
	
	private StatusEnum status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Usuario getUsuarioMudouStatus() {
		return usuarioMudouStatus;
	}

	public void setUsuarioMudouStatus(Usuario usuarioMudouStatus) {
		this.usuarioMudouStatus = usuarioMudouStatus;
	}

	public Date getDataMudouStatus() {
		return dataMudouStatus;
	}

	public void setDataMudouStatus(Date dataMudouStatus) {
		this.dataMudouStatus = dataMudouStatus;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}
	
	
	
}
